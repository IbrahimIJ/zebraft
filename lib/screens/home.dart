import 'dart:io';

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:webview_flutter/webview_flutter.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late WebViewController controller;
  _launchURL(String url) async {
    if (await canLaunchUrlString(url)) {
      await launchUrlString(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> _launchInBrowser(Uri url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.externalNonBrowserApplication,
    )) {
      throw 'Could not launch $url';
    }
  }

  Future<void> _launchInWebViewWithoutDomStorage(Uri url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.platformDefault,
      webViewConfiguration: const WebViewConfiguration(enableDomStorage: true),
    )) {
      throw 'Could not launch $url';
    }
  }

  int position = 1;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (Platform.isAndroid) {
      WebView.platform = AndroidWebView();
    } else {
      WebView.platform = CupertinoWebView();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Platform.isIOS
          ? AppBar(
              actions: [
                IconButton(
                  onPressed: () async {
                    if (await controller.canGoBack()) {
                      controller.goBack();
                    }
                  },
                  icon: Icon(Icons.arrow_back),
                ),
                IconButton(
                  onPressed: () {
                    controller.reload();
                  },
                  icon: Icon(Icons.refresh),
                ),
              ],
              title: Text("Zebra FT"),
              centerTitle: true,
              backgroundColor: Colors.black,
            )
          : AppBar(
              title: Text("Zebra FT"),
              centerTitle: true,
              backgroundColor: Colors.black,
            ),
      body: IndexedStack(
        index: position,
        children: [
          Container(
            child: Center(child: CircularProgressIndicator()),
          ),
          WebView(
            javascriptMode: JavascriptMode.unrestricted,
            initialUrl: 'https://zebraft.wl-solutions.net/',
            onWebViewCreated: (controller) {
              this.controller = controller;
            },
            navigationDelegate: (NavigationRequest request) {
              if (request.url.startsWith("https://zebraft.wl-solutions.net/")) {
                if (request.url.endsWith('.pdf')) {
                  // _launchURL(request.url);
                  if (Platform.isAndroid) {
                    _launchInBrowser(Uri.parse(request.url));
                    return NavigationDecision.prevent;
                  }
                  return NavigationDecision.navigate;
                }
                return NavigationDecision.navigate;
              } else {
                // _launchURL(request.url);
                if (Platform.isAndroid) {
                  _launchInBrowser(Uri.parse(request.url));
                  return NavigationDecision.prevent;
                } else {
                  return NavigationDecision.navigate;
                }
              }
            },
          ),
        ],
      ),
    );
  }
}
